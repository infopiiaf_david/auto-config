# Auto-Config scripts

## Pre-requisites…

0. Install Git: `sudo apt-get install git`;
0. Clone this repository: `git clone <repo> ~/.auto-config`;
0. Change directory to `~/.auto-config`;
0. [Install Ansible](http://docs.ansible.com/ansible/intro_installation.html#latest-releases-via-apt-ubuntu).

## Play around!

```
ansible-playbook ansible/playbook.yml -i 'localhost,' -c local -K
```
